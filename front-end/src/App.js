import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import LoginView from "./CustomComponents/LoginView";
import RegisterView from "./CustomComponents/RegisterView";
import HomeView from "./CustomComponents/HomeView";
import PasswordResetView from "./CustomComponents/PasswordResetView";
import CreateBudgetView from "./CustomComponents/CreateBudgetView";
import CreateGroupView from "./CustomComponents/CreateGroupView";
import MyAccountView from "./CustomComponents/MyAccountView";
import VerifyMailView from "./CustomComponents/VerifyMailView";
import ChangePasswordView from "./CustomComponents/ChangePasswordView";
import NavbarView from "./CustomComponents/NavbarView";

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      nameUser: "Name",
      surnameUser: "Surname",
      emailUser: "email",
      id: -1,
      verified: 0,
    };
  }

  QSetUser = (userData) => {
    this.setState({
        isLoggedIn: true,
        userStatus: { logged: true, user: [userData] },
        nameUser: userData.name,
        surnameUser: userData.surname,
        emailUser: userData.email,
        id: userData.id,
        verified: userData.verified
    });
};

  handleLogin = () => {
    this.setState({ isLoggedIn: true });
  };

  handleLogout = () => {
    this.setState({ isLoggedIn: false });
    alert("You've logged out!")
  };

  render() {
    return (
      <>
        <BrowserRouter>
            <NavbarView  isLoggedIn={this.state.isLoggedIn} onLogout={this.handleLogout} />
            <Routes>
              <Route path="/" element={<HomeView />} />
              <Route path="/registeracc" element={<RegisterView />} />
              <Route path="/loginacc" element={<LoginView onLogin={this.handleLogin} 
               onLogout={this.handleLogout} QUserFromChild={this.QSetUser} 
               name={this.state.nameUser} surname={this.state.surnameUser}/>} />
              <Route path="/resetpw" element={<PasswordResetView />} />
              <Route path="/create" element={this.state.isLoggedIn ? <CreateBudgetView id={this.state.id} /> : <Navigate to="/loginacc" />} />
              <Route path="/group"  element={this.state.isLoggedIn ? <CreateGroupView /> : <Navigate to="/loginacc" />} />
              <Route path="/myaccount" element={this.state.isLoggedIn ? <MyAccountView name={this.state.nameUser}
                                surname={this.state.surnameUser} email={this.state.emailUser} id={this.state.id} /> : <Navigate to="/loginacc" /> } />
              <Route path="/verifymail" element={<VerifyMailView email={this.state.emailUser} />} />
              <Route path="/changepw" element={<ChangePasswordView id={this.state.id} />} />
            </Routes>
          </BrowserRouter>
      </>
    )
  }
}

export default App;
