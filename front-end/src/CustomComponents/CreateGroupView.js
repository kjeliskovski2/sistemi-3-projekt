import React from "react";
import "../Styles/CreateGroupView.css";

class CreateGroupView extends React.Component {
    render() {
        return(
            <>
                <div className="groupCreate">
                    <h1 id="createTitle">Create a new group</h1>
                    <div className="groupDiv">
                        <div className="innerGroupCreate">
                            <form className="row g-3">
                                <div className="col-md-6">
                                    <label htmlFor="name" className="form-label">
                                    Name
                                    </label>
                                    <input type="text" className="form-control" id="inputEmail4" placeholder="Enter the name of the group" />
                                </div>
                                <div className="col-md-3">
                                    <label htmlFor="inputZip" className="form-label">
                                    No. of users
                                    </label>
                                    <input type="number" className="form-control" id="inputZip" placeholder="ex. 3" />
                                </div>
                                <div className="col-md-5">
                                    <label htmlFor="inputZip" className="form-label">
                                    If the group has budget, enter budget ID
                                    </label>
                                    <input type="number" className="form-control" id="inputZip" placeholder="Budget ID"/>
                                </div>
                                <div className="col-12">
                                    <button id="createGButton" type="submit" className="btn btn-primary">
                                    Create
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default CreateGroupView;