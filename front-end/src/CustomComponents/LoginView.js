import React from 'react';
import { Link, Navigate } from "react-router-dom";
import "../Styles/LoginView.css";
import axios from 'axios';
import PropTypes from 'prop-types';

class LoginView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: { type: "login" },
            incorrect: false,
            logged: false
        }
    }

    QGetTextFromField = (e) => {
        this.setState(prevState => ({
            user: { ...prevState.user, [e.target.name]: e.target.value }
        }))
    }

    QPostLogin = () => {
        axios.post('http://88.200.63.148:8092/login', {
            email: this.state.user.email,
            password: this.state.user.password,
        })
            .then(response => {
                console.log("Sent to server...")
                console.log(response.status)
                if (response.status === 200) {
                    const userData = response.data.user;
                    this.props.onLogin();
                    this.props.QUserFromChild(userData);
                    this.setState({incorrect: false});
                    this.setState({logged: true});
                } else if (response.status === 204) {
                    this.setState({ incorrect: true });
                } else {
                    console.log("Something is really wrong, DEBUG!")
                }
            })
            .catch(err => {
                console.log(err)
            })
    };

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.QPostLogin();
        }
    };
     
    render() {

        return(
            <div>
                    <h1 id="title">Login into your account</h1>
                    {this.state.incorrect && (
                        <h2 id="incorrectC">Incorrect credentials, try again!</h2>
                    )}
                    <section className="vh-100">
                        <div className="container-fluid h-custom">
                            <div className="row d-flex justify-content-center align-items-center h-100">
                            <div className="col-md-9 col-lg-6 col-xl-5">
                                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
                                className="img-fluid" alt="Sample image" />
                            </div>
                            <div className="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                                <form onKeyPress={this.handleKeyPress}>
                                <div className="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
                                    <p className="lead fw-normal mb-0 me-3">Sign in with</p>
                                    <button type="button" className="btn btn-primary btn-floating mx-1">
                                    Google
                                    </button>

                                    <button type="button" className="btn btn-primary btn-floating mx-1">
                                    Apple
                                    </button>

                                    <button type="button" className="btn btn-primary btn-floating mx-1">
                                    GitHub
                                    </button>
                                </div>

                                <div className="divider d-flex align-items-center my-4">
                                    <p className="text-center fw-bold mx-3 mb-0">Or</p>
                                </div>

                                <div className="form-outline mb-4">
                                    <input name="email" type="email" id="form3Example3" className="form-control form-control-lg"
                                    placeholder="Enter your email address" 
                                    onChange={(e)=>this.QGetTextFromField(e)}
                                    />
                                    <label className="form-label" htmlFor="form3Example3">Email address</label>
                                </div>

                                
                                <div className="form-outline mb-3">
                                    <input name="password" type="password" id="form3Example4" className="form-control form-control-lg"
                                    placeholder="Enter password" 
                                    onChange={(e)=>this.QGetTextFromField(e)}
                                    />
                                    <label className="form-label" htmlFor="form3Example4">Password</label>
                                </div>

                                <div className="d-flex justify-content-between align-items-center">
                                
                                    <div className="form-check mb-0">
                                    <input className="form-check-input me-2" type="checkbox" value="" id="form2Example3" />
                                    <label className="form-check-label" htmlFor="form2Example3">
                                        Remember me
                                    </label>
                                    </div>
                                    <Link to="/resetpw" className="text-body">Forgot password?</Link>
                                </div>

                                <div className="text-center text-lg-start mt-4 pt-2">
                                    <button type="button" className="btn btn-primary btn-lg"
                                        style={{paddingLeft: "2.5rem", paddingRight: "2.5rem"}}
                                        onClick={this.QPostLogin}
                                        >Login</button>
                                    <p className="small fw-bold mt-2 pt-1 mb-0">Dont have an account? <Link to="/registeracc"
                                        className="link-danger">Register</Link></p>
                                </div>

                                </form>
                            </div>
                            </div>
                        </div>
                        <div id="botBanner" className="d-flex flex-column flex-md-row text-center text-md-start justify-content-between py-4 px-4 px-xl-5 bg-primary" >
                            
                            <div className="text-white mb-3 mb-md-0">
                            Student Budget Tracker © 2023. All rights reserved.
                            </div>
                        
                        </div>
                    </section>
                    {this.state.logged && <Navigate to="/myaccount" />}
        </div>
        )
    }

}

LoginView.propTypes = {
    QSetUser : PropTypes.func.isRequired,
    QUserFromChild: PropTypes.func.isRequired,
    onLogin: PropTypes.func.isRequired
  };

export default LoginView