import React from 'react';
import '../Styles/RegisterView.css';
import axios from 'axios';
import { Navigate } from "react-router-dom";

class RegisterView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: { type: "register" },
            registered: false
        }
    }

    QGetTextFromField = (e) => {
        this.setState(prevState => ({
            user: { ...prevState.user, [e.target.name]: e.target.value }
        }))
    }

    QPostRegister = (e) => {
        e.preventDefault();

        axios.post('http://88.200.63.148:8092/signup', {
            name: this.state.user.name,
            surname: this.state.user.surname,
            email: this.state.user.email,
            password: this.state.user.password,
        })
        .then(response => {
            console.log("Sent to server...")
            console.log(response.status);
            if(response.status === 204) { this.setState({registered: false});}
            else {this.setState({registered: true});}
        })
        .catch(err => {
            console.log(err);
            this.setState({registered: false});
        })
    };

    render() {
        return(
            <>  
                <section id="section" className="text-center">
     
                    <div className="p-5 bg-image" style={{
                    backgroundImage: "url('https://png.pngtree.com/thumb_back/fh260/background/20211118/pngtree-financial-technology-data-graph-image_908921.jpg')",
                    height: "300px"
                    }}></div>
            

                    <div className="card mx-4 mx-md-5 shadow-5-strong" style={{
                    marginTop: "-100px",
                    background: "hsla(0, 0%, 100%, 0.8)",
                    backdropFilter: "blur(30px)"
                    }}>
                    
                    <div id="cardBody" className="card-body py-5 px-md-5">

                    <div className="row d-flex justify-content-center">
                            <div className="col-lg-8">
                                <h2 className="fw-bold mb-5">Sign up now</h2>
                                <form>
                            
                                    <div className="row">
                                        <div className="col-md-6 mb-4">
                                            <div className="form-outline">
                                            <input onChange={(e)=>this.QGetTextFromField(e)} name="name" type="text" id="form3Example1" className="form-control" />
                                            <label className="form-label" htmlFor="form3Example1">First name</label>
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-4">
                                        <div className="form-outline">
                                        <input onChange={(e)=>this.QGetTextFromField(e)} name="surname" type="text" id="form3Example2" className="form-control" />
                                        <label className="form-label" htmlFor="form3Example2">Last name</label>
                                        </div>
                                    </div>
                            </div>

                        
                                <div className="form-outline mb-4">
                                <input onChange={(e)=>this.QGetTextFromField(e)} name="email" type="email" id="form3Example3" className="form-control" />
                                <label className="form-label" htmlFor="form3Example3">Email address</label>
                                </div>

                                <div className="form-outline mb-4">
                                <input onChange={(e)=>this.QGetTextFromField(e)} name="password" type="password" id="form3Example4" className="form-control" />
                                <label className="form-label" htmlFor="form3Example4">Password</label>
                                </div>

                                <div className="form-check d-flex justify-content-center mb-4">
                                <input className="form-check-input me-2" type="checkbox" value="" id="form2Example33"/>
                                <label className="form-check-label" htmlFor="form2Example33">
                                    Subscribe to our newsletter
                                </label>
                                </div>

                        
                                <button onClick={this.QPostRegister} type="submit" className="btn btn-primary btn-block mb-4">
                                Sign up
                                </button>

                            
                                <div className="text-center">
                                <p>or sign up with:</p>
                                <button type="button" className="btn btn-link btn-floating mx-1">
                                    Google
                                </button>

                                <button type="button" className="btn btn-link btn-floating mx-1">
                                   Apple
                                </button>

                                <button type="button" className="btn btn-link btn-floating mx-1">
                                    GitHub
                                </button>

                                <button type="button" className="btn btn-link btn-floating mx-1">
                                    Outlook
                                </button>
                                </div>
                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>

                {this.state.registered && <Navigate to="/loginacc" />}
            </>
        )
    }
}

export default RegisterView;