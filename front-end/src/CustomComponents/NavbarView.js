import React from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import "../Styles/styles.css";

class NavbarView extends React.Component {

    static propTypes = {
      isLoggedIn: PropTypes.bool.isRequired,
      onLogout: PropTypes.func.isRequired
    };

    handleLogout = () => {
      this.props.onLogout();
    };

    render() {
      const { isLoggedIn } = this.props;
      return (
        <>
          <nav id="nav" className="navbar navbar-expand-lg bg-body-tertiary">
                          <div className="container-fluid">
                              <Link to="/" className="btn btn-primary" id="btn">Home</Link>
                              <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                              <span className="navbar-toggler-icon"></span>
                              </button>
                              <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                                <div className="navbar-nav">
                                    <Link to="/myaccount" className="btn btn-primary" id="btn" aria-current="page">My account</Link>
                                    <Link to="/create" className="btn btn-primary" id="btn">Create a budget</Link>
                                    <Link to="/group" className="btn btn-primary" id="btn">Create a group</Link>
                                    <button className="btn btn-primary" id="btn">Join a group</button>
                                    {isLoggedIn == true ? (
                                    <button  className="btn btn-success" id="loginBtn" aria-disabled="true" onClick={this.handleLogout}>Logout</ button>                                    ): (
                                      <Link to="/loginacc"  className="btn btn-success" id="loginBtn" aria-disabled="true">Login</Link>
                                    )}
                                </div>
                              </div>
                          </div>    
                  </nav>
        </>
      )
    }
  }
  
  export default NavbarView;