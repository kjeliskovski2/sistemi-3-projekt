import React from "react";
import axios from "axios";
import PropTypes from 'prop-types';
import "../Styles/CreateBudgetView.css";
import { Navigate } from "react-router-dom";

class CreateBudgetView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            budget: {},
            created: false
        }
    }

    static propTypes = {
        id: PropTypes.string.isRequired,
    };

    QGetTextFromField = (e) => {
        this.setState(prevState => ({
            budget: { ...prevState.budget, [e.target.name]: e.target.value }
        }))
    }

    QPostBudget = (e) => {
        e.preventDefault();
        const { id } = this.props;

        axios.post('http://88.200.63.148:8092/getbudget/create', {
            name: this.state.budget.name,
            n_cat: this.state.budget.n_cat,
            expires: this.state.budget.expires,
            amount: this.state.budget.amount,
            id: id
        })
        .then(response => {
            console.log("Budget sent...")
            console.log(response.status);
            this.setState({created: true})
        })
        .catch(err => {
            console.log(err)
            this.setState({created: false})
        })
    };


    render() {
        return(
            <>
                <div className="outerCreate">
                    <h1 id="createTitle">Create a new budget</h1>
                    <div className="createDiv">
                        <div className="innerCreate">
                            <form className="row g-3">
                                <div className="col-md-6">
                                    <label htmlFor="name" className="form-label">
                                    Name
                                    </label>
                                    <input onChange={(e)=>this.QGetTextFromField(e)} name="name" type="text" className="form-control" id="inputEmail4" placeholder="Enter the name of the budget" />
                                </div>
                                <div className="col-md-3">
                                    <label htmlFor="inputZip" className="form-label">
                                    No. of categories
                                    </label>
                                    <input onChange={(e)=>this.QGetTextFromField(e)} name="n_cat" type="number" className="form-control" id="inputZip" placeholder="ex. 3" />
                                </div>
                                <div className="col-9">
                                    <label htmlFor="inputAddress" className="form-label">
                                    Date of expire
                                    </label>
                                    <input
                                    onChange={(e)=>this.QGetTextFromField(e)} name="expires"
                                    type="date"
                                    className="form-control"
                                    id="inputAddress"
                                    />
                                </div>
                                <div className="col-9">
                                    <label htmlFor="inputAddress2" className="form-label">
                                    Starting amount of money
                                    </label>
                                    <input
                                    onChange={(e)=>this.QGetTextFromField(e)} name="amount"
                                    type="number"
                                    className="form-control"
                                    id="inputAddress2"
                                    placeholder="Enter your amount of money, ex. 500"
                                    />
                                </div>
                                <div className="col-md-5">
                                    <label htmlFor="inputZip" className="form-label">
                                    If its a group budget, enter group ID If not, leave it empty
                                    </label>
                                    <input type="number" className="form-control" id="inputZip" placeholder="Group ID"/>
                                </div>
                                <div className="col-12">
                                    <button onClick={this.QPostBudget} id="createButton" type="submit" className="btn btn-primary">
                                    Create
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                {this.state.created && <Navigate to="/myaccount" />}
            </>
        )
    }
}

export default CreateBudgetView;