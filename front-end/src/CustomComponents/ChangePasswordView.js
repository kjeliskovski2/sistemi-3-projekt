import React from "react";
import PropTypes from 'prop-types';
import axios from "axios";
import { Navigate } from "react-router-dom";
import "../Styles/ChangePasswordView.css"

class ChangePasswordView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          passwords:{},
          changeSuccessful: false,
          incorrectPw: false
        };
    }

    static propTypes = {
        id: PropTypes.string.isRequired
    };

    QGetTextFromField=(e)=>{
        this.setState(prevState=>({
            passwords:{...prevState.passwords,[e.target.name]:e.target.value}
            }))
    }

    QChangePw=()=>{

        const { id }       = this.props;
        const oldPassword  = this.state.passwords.oldPassword;
        const newPassword  = this.state.passwords.newPassword;

        axios.post('http://88.200.63.148:8092/reset/change', {
            id,
            oldPassword,
            newPassword
        })
        .then(response => {
            if (response.status === 200) {
                this.setState({ changeSuccessful: true });
                this.setState({incorrectPw: false});
                alert("Password changed successfully!")
            } else if (response.status === 204) {
                this.setState({ incorrectPw: true });
                this.setState({changeSuccessful: false});
            } else {
                console.log("Something is really wrong, DEBUG!")
            }
        }) 
       
    }

    render() {
        return (
             <>
                <div className="chBackDiv">
                <h4 id="ChangePwTitle">Change your password</h4>
                {this.state.incorrectPw && <h4 style={{color: 'red'}}>Your old password is incorret!</h4>}
                    <div className="chInnerDiv">
                    <form>
                            <div className="mb-3">
                              <label htmlFor="exampleInputEmail1" className="form-label">
                                Please enter the following data
                              </label>
                              <input
                                name="oldPassword"
                                onChange={(e)=>this.QGetTextFromField(e)}
                                type="password"
                                className="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                placeholder="Enter your old password"
                              />
                              <input
                                name="newPassword"
                                onChange={(e)=>this.QGetTextFromField(e)}
                                type="password"
                                className="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                placeholder="Enter your new password"
                              />
                            </div>
                            <button
                              
                              type="button"
                              className="btn btn-primary"
                              onClick={this.QChangePw}
                              style={{marginLeft: "5vw"}}
                            >
                              Change password
                            </button>
                        </form>
                    </div>
                </div>

                {this.state.changeSuccessful && <Navigate to ="/myaccount" />}
             </>
        );
    }
}

export default ChangePasswordView