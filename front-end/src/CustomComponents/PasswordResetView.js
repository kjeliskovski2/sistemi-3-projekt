import React from 'react';
import '../Styles/PasswordResetView.css';
import axios from 'axios';

class PasswordResetView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          info:{},
          resetSuccessful: false
        };
    }

    QGetTextFromField=(e)=>{
        this.setState(prevState=>({
            info:{...prevState.info,[e.target.name]:e.target.value}
            }))
    }
    

    QPostMail=()=>{
        axios.post('http://88.200.63.148:8092/reset',{
            email:this.state.info.email
        })
        .then(response=>{
            console.log("Sent to server...",response);
            this.setState({ resetSuccessful: true });
        })
        .catch(err=>{
            console.log(err)
        })
    }

    render() {
        return (
            <>
                <div className="backgroundDiv">
                    <h1 id="resetTitle">Reset your password</h1>
                    <div className="innerDiv">
                        {this.state.resetSuccessful ? (
                          <p>Password reset successful! Check your email, it will take few minutes.</p>
                        ) : (
                        <form>
                            <div className="mb-3">
                              <label htmlFor="exampleInputEmail1" className="form-label">
                                Email address
                              </label>
                              <input
                                name="email"
                                onChange={(e) => this.QGetTextFromField(e)}
                                type="email"
                                className="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                              />
                              <div id="emailHelp" className="form-text">
                                Enter the email that you have registered.
                              </div>
                            </div>
                            <button
                              onClick={() => this.QPostMail()}
                              type="button"
                              className="btn btn-primary"
                            >
                              Send reset email
                            </button>
                        </form>
                    )}
                    </div>
                </div>
            </>
        );
    }
}

export default PasswordResetView;