import React from 'react';
import PropTypes from 'prop-types';
import "../Styles/MyAccountView.css";
import budgetImage from "../Images/bank.jpg";
import { Link } from "react-router-dom";
import axios from 'axios';
import { jsPDF } from 'jspdf';
import imageURL from "../Images/logo.png"

class MyAccountView extends React.Component {

    constructor(props)
    {
        super(props);
        this.state={
            budget:[],
            verified: 0,
            searchItem: "",
            budgetLog: {}
        }
    }


    static propTypes = {
        name: PropTypes.string.isRequired,
        surname: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        id: PropTypes.number.isRequired
    };

    handleSearchChange = (event) => {
        this.setState({ searchItem: event.target.value });
      };
      

    startDataFetching = () => {
        this.fetchBudgetData();

        this.intervalId = setInterval(this.fetchBudgetData, 3000);
    };

    fetchBudgetData = () => {
        const { id } = this.props;

        axios.post('http://88.200.63.148:8092/getbudget', { id: id })
            .then(response => {
                this.setState({budget: response.data.queryResult})
                this.setState({verified: response.data.queryResult2[0].verified})
                
            })
            .catch(err => {
                console.log("Error" + err);
            });
    };

    componentDidMount() {
        this.startDataFetching();
    }

    componentWillUnmount() {
        clearInterval(this.intervalId);
    }

    QPostMail=()=>{
        const { email } = this.props;

        axios.post('http://88.200.63.148:8092/verify',{
            email: email
        })
        .then(response=>{
            console.log("Sent to server...",response);
            this.setState({ resetSuccessful: true });
        })
        .catch(err=>{
            console.log(err)
        })
    }

    makeLog = (b_id) => {
        const id = this.props.id;

        console.log("b_id:"+b_id+" u_id:"+id)

        axios.post('http://88.200.63.148:8092/getbudget/log', { 
            u_id: id,
            b_id: b_id
        })
        .then(response => {
            const fetchedBudget = response.data.queryResult[0];
            this.setState({ budgetLog: fetchedBudget }, () => {
                this.generatePDF(this.state.budgetLog);
            });      
            })
            .catch(err => {
                console.log("Error" + err);
        });
          
    }

    generatePDF = (b) => {
        const doc         = new jsPDF();
        const date        = new Date(b.date);
        const date2       = new Date(b.expires);
        const createdDate = date.toLocaleDateString();
        const expiresDate = date2.toLocaleDateString();

        doc.addImage(imageURL, "PNG", 45, 0, 100, 30)
        doc.text("Student budget tracker - Report ", 60, 35);
        doc.text("Report for budget: "+b.name, 10, 60)
        doc.text("Created on: "+createdDate, 10,70)
        doc.text("Expiring on: "+expiresDate, 10, 80)
        doc.text("With starting amount of money: "+b.amount+"€", 10, 90)
        doc.text("Based on the expenditure of the user the total amount of money spent at ", 10, 110)
        doc.text("the moment of generating this report is: ", 10, 120)
        doc.text("Here the amount of money will be displayed with the expenses", 25, 140)
        doc.text("Prepared by: Student budget tracker team", 80 , 230 )
        doc.save('BudgetReport.pdf');
          
    };


    render() {
        const { name, surname } = this.props;
        const { verified, searchItem } = this.state;
        const budgets = this.state.budget;
        const filteredBudgets = budgets.filter((item) =>
            item.name.toLowerCase().includes(searchItem.toLowerCase())
        );

        return(
            <>
            <div className="parentDiv">
                <div className='sideBar'>
                    <div className='name'>
                        <svg id="svg" xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" className="bi bi-person" viewBox="0 0 16 16">
                            <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664z"/>
                        </svg>
                        {verified > 0 ? 
                        <>
                        <h4 id="name">{name} {surname} </h4>
                        <svg style={{marginLeft: "0.4vw", marginTop:"-0.4vw"}} xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0m-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                        </svg>
                        </>
                         : <h4 id="name">{name} {surname}</h4>}
                        
                    </div>
                    <hr />
                    <div className='sideDiv'>
                        <h4 id="discTitle">DISCOVER</h4>
                        <ul>
                            <li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-person-lines-fill" viewBox="0 0 16 16">
                                <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5m.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1z"/>
                                </svg> My friends
                            </li>
                            <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-people-fill" viewBox="0 0 16 16">
                            <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                            </svg> My groups
                            </li>
                            <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-cash-coin" viewBox="0 0 16 16">
                            <path fillRule="evenodd" d="M11 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8m5-4a5 5 0 1 1-10 0 5 5 0 0 1 10 0"/>
                            <path d="M9.438 11.944c.047.596.518 1.06 1.363 1.116v.44h.375v-.443c.875-.061 1.386-.529 1.386-1.207 0-.618-.39-.936-1.09-1.1l-.296-.07v-1.2c.376.043.614.248.671.532h.658c-.047-.575-.54-1.024-1.329-1.073V8.5h-.375v.45c-.747.073-1.255.522-1.255 1.158 0 .562.378.92 1.007 1.066l.248.061v1.272c-.384-.058-.639-.27-.696-.563h-.668zm1.36-1.354c-.369-.085-.569-.26-.569-.522 0-.294.216-.514.572-.578v1.1h-.003zm.432.746c.449.104.655.272.655.569 0 .339-.257.571-.709.614v-1.195l.054.012z"/>
                            <path d="M1 0a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4.083c.058-.344.145-.678.258-1H3a2 2 0 0 0-2-2V3a2 2 0 0 0 2-2h10a2 2 0 0 0 2 2v3.528c.38.34.717.728 1 1.154V1a1 1 0 0 0-1-1z"/>
                            <path d="M9.998 5.083 10 5a2 2 0 1 0-3.132 1.65 5.982 5.982 0 0 1 3.13-1.567z"/>
                            </svg> Previous budgets
                            </li>
                        </ul>
                    </div>
                    <div className='sideDiv'>
                        <h4 id="discTitle">SOCIAL</h4>
                        <ul>
                            <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-youtube" viewBox="0 0 16 16">
                            <path d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.123c.002-.215.01-.958.064-1.778l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408z"/>
                            </svg> How to start?
                            </li>
                            <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-share" viewBox="0 0 16 16">
                            <path d="M13.5 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3M11 2.5a2.5 2.5 0 1 1 .603 1.628l-6.718 3.12a2.499 2.499 0 0 1 0 1.504l6.718 3.12a2.5 2.5 0 1 1-.488.876l-6.718-3.12a2.5 2.5 0 1 1 0-3.256l6.718-3.12A2.5 2.5 0 0 1 11 2.5m-8.5 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3m11 5.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3"/>
                            </svg> Invite friends 
                            </li>
                        </ul>
                    </div>
                    <div className='sideDiv'>
                        <h4 id="discTitle">ACCOUNT</h4>
                        <ul>
                        <li onClick={this.QPostMail}><Link to="/verifymail" style={{ textDecoration: 'none', color: 'rgb(114, 111, 107)' }}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-envelope-at-fill" viewBox="0 0 16 16">
                                <path d="M2 2A2 2 0 0 0 .05 3.555L8 8.414l7.95-4.859A2 2 0 0 0 14 2zm-2 9.8V4.698l5.803 3.546L0 11.801Zm6.761-2.97-6.57 4.026A2 2 0 0 0 2 14h6.256A4.493 4.493 0 0 1 8 12.5a4.49 4.49 0 0 1 1.606-3.446l-.367-.225L8 9.586l-1.239-.757ZM16 9.671V4.697l-5.803 3.546.338.208A4.482 4.482 0 0 1 12.5 8c1.414 0 2.675.652 3.5 1.671"/>
                                <path d="M15.834 12.244c0 1.168-.577 2.025-1.587 2.025-.503 0-1.002-.228-1.12-.648h-.043c-.118.416-.543.643-1.015.643-.77 0-1.259-.542-1.259-1.434v-.529c0-.844.481-1.4 1.26-1.4.585 0 .87.333.953.63h.03v-.568h.905v2.19c0 .272.18.42.411.42.315 0 .639-.415.639-1.39v-.118c0-1.277-.95-2.326-2.484-2.326h-.04c-1.582 0-2.64 1.067-2.64 2.724v.157c0 1.867 1.237 2.654 2.57 2.654h.045c.507 0 .935-.07 1.18-.18v.731c-.219.1-.643.175-1.237.175h-.044C10.438 16 9 14.82 9 12.646v-.214C9 10.36 10.421 9 12.485 9h.035c2.12 0 3.314 1.43 3.314 3.034zm-4.04.21v.227c0 .586.227.8.581.8.31 0 .564-.17.564-.743v-.367c0-.516-.275-.708-.572-.708-.346 0-.573.245-.573.791Z"/>
                                </svg> Verify email</Link>
                            </li>
                            <Link to="/changepw" style={{ textDecoration: 'none', color: 'rgb(114, 111, 107)' }}><li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-key" viewBox="0 0 16 16">
                                <path d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8m4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5"/>
                                <path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0"/>
                                </svg> Change password
                            </li></Link>
                            <li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-trash-fill" viewBox="0 0 16 16">
                                <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5M8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5m3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0"/>
                                </svg> Delete your account
                            </li>
                            <li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-info-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
                                <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0"/>
                                </svg> Contact an admin
                            </li>
                        </ul>
                    </div>
                </div>
                <div className='middleBar'>
                    <div className='inputDiv'>
                        <div id="inGroup" className="input-group">
                            <div id="inputGroup" className="form-outline" data-mdb-input-init>
                                <input type="search" id="form1" className="form-control" 
                                value={searchItem}
                                onChange={this.handleSearchChange}
                                placeholder='Search for a budget, group, etc..' />
                            </div>
                            <button type="button" className="btn btn-primary" data-mdb-ripple-init>
                                <i className="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <div className='cardDiv'>

                    {searchItem !== "" ? (
                        filteredBudgets.length > 0 ? (
                            filteredBudgets.map((item) => {
                                const expiryDate = new Date(item.expires);
                                return (
                                    <div className="card" key={item.id} id="card1">
                                        <img src={budgetImage} className="card-img-top" alt="Budget" />
                                        <div className="card-body">
                                            <h5 className="card-title">Budget: {item.name}</h5>
                                            <h6 className="card-text">ID: {item.id}</h6>
                                            <h6 className="card-text">Total amount: {item.amount}€</h6>
                                            <h6 className="card-text">Date of expiry: {expiryDate.toLocaleDateString()}</h6>
                                            <h6 className="card-text">Number of categories: {item.n_cat}</h6>
                                            {item.g_id !== null ? (<h6 className="card-text">Group ID: {item.g_id}</h6>
                                            ): <h6 className="card-text">Not a group budget</h6>}
                                            <div className='btnDiv'>
                                                <button id="checkBtn" className="btn btn-primary" style={{ fontSize:'0.8vw' }}
                                                >Check budget</button>
                                                <button id="checkBtn" className="btn btn-primary" style={{ marginLeft: "2vw", fontSize:'0.8vw' }}
                                                onClick={() => this.makeLog(item.id)}
                                                >Make a log</button>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })
                        ) : (
                            <h2>No matching budgets found</h2>
                        )
                    ) : (
                        budgets.length > 0 ? (
                            budgets.map((item) => {
                                const expiryDate = new Date(item.expires);
                                return (
                                    <div className="card" key={item.id} id="card1">
                                        <img src={budgetImage} className="card-img-top" alt="Budget" />
                                        <div className="card-body">
                                            <h5 className="card-title">Budget: {item.name}</h5>
                                            <h6 className="card-text">Total amount: {item.amount}€</h6>
                                            <h6 className="card-text">Date of expiry: {expiryDate.toLocaleDateString()}</h6>
                                            <h6 className="card-text">Number of categories: {item.n_cat}</h6>
                                            {item.g_id !== null ? (<h6 className="card-text">Group ID: {item.g_id}</h6>
                                            ): <h6 className="card-text">Not a group budget</h6>}
                                            <div className='btnDiv'>
                                                <button id="checkBtn" className="btn btn-primary" style={{ fontSize:'0.8vw' }}
                                                >Check budget</button>
                                                <button id="checkBtn" className="btn btn-primary" style={{ marginLeft: "2vw", fontSize:'0.8vw' }}
                                                onClick={() => this.makeLog(item.id)}
                                                >Make a log</button>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })
                        ) : (
                            <h2>You dont have any budgets</h2>
                        )
                    )}


                        
                    </div>
                </div>
            </div>
            </>
        )
    }
}

export default MyAccountView;