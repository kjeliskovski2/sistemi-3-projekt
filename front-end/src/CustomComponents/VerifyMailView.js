import React from "react";
import axios from "axios";
import PropTypes from 'prop-types';
import "../Styles/VerifyMailView.css";
import { Navigate } from "react-router-dom";

class VerifyMailView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          info:{},
          verifySuccessful: false,
          incorrectCode: false
        };
    }

    static propTypes = {
        email: PropTypes.string.isRequired
    };

    QGetTextFromField=(e)=>{
        this.setState(prevState=>({
            info:{...prevState.info,[e.target.name]:e.target.value}
            }))
    }

    QPostCode=()=>{

        const { email } = this.props;
        const verificationCode = parseInt(this.state.info.code, 10);

        axios.post('http://88.200.63.148:8092/verify/code',{
            code: verificationCode,
            email: email
        })
        .then(response=>{

            if (response.status === 200) {
                console.log("Sent to server...",response);
                this.setState({ verifySuccessful: true });
                this.setState({incorrectCode: false});
                alert("Email verifed!")
            } else if (response.status === 204) {
                this.setState({ incorrectCode: true });
                this.setState({verifySuccessful: false});
            } else {
                console.log("Something is really wrong, DEBUG!")
            }

            
        })
        .catch(err=>{
            this.setState({ verifySuccessful: false });
            this.setState({incorrectCode: true});
            console.log(err)
        })
    }


    render() {
        return (
            <>
                <div className="VerBackgDiv">
                    <h4 id="VerifyTitle">Enter the verification code</h4>
                    {this.state.incorrectCode && (
                        <h4 style={{color: 'red'}}>Incorrect code, try again!</h4>
                    )}
                    <div className="innerVerDiv">
                    {this.state.verifySuccessful ? (
                          <p>Verification successful.</p>
                        ) : (
                        <form>
                            <div className="mb-3">
                              <label htmlFor="exampleInputEmail1" className="form-label">
                                Verification code
                              </label>
                              <input
                                name="code"
                                onChange={(e)=>this.QGetTextFromField(e)}
                                type="number"
                                className="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                              />
                              <div id="emailHelp" className="form-text">
                               Enter the code that was sent to your email. It will take few minutes.
                              </div>
                            </div>
                            <button
                              
                              type="button"
                              className="btn btn-primary"
                              onClick={this.QPostCode}
                            >
                              Verify email
                            </button>
                        </form>
                        )}
                    </div>
                </div>
                {this.state.verifySuccessful && <Navigate to="/myaccount" />}
            </>
        );
    }

}

export default VerifyMailView;