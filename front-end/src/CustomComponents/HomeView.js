import React from 'react'
import "../Styles/HomeView.css"
import { Link } from "react-router-dom";

class HomeView extends React.Component {

    render() {
        return(
            <>
                <div className='outer'>
                    <div className='inner'>
                        <h1 className='title'>Budget Tracker</h1>
                        <p id="paragraph" className='text'>Track all of your finances and expenses with one simple click.  <br />
                        Create, edit and interact with different budgets and set up goals.<br /> Manage your expenses by creating different categories.</p>
                        <p id="paragraph">Create a group budget for your family or job and set your goals.</p> <br />
                        <p id="paragraph" className='last'>Join us now and start saving!</p>
                        <Link to="/registeracc"><button id="button" type="button" className="btn btn-danger">JOIN NOW</button></Link>
                    </div>
                </div>
                
                
            </>
        )
    }

}

export default HomeView