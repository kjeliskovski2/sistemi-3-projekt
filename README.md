# Student Budget Tracker - Information System



## Project Description

This project is an information system called "Student Budget Tracker". Its purpose is budget managing and tracking for those who are not skilled at finance tracking. 

## System functions

The IS has a broad range of functions, some of them are listed below.

- [ ] Registration - allows the user to create a new account into the system
- [ ] Login - allows the user to login into the system
- [ ] Email verification - the user can verify their email for safer use of the IS
- [ ] Password reset - at any given time the password can be reseted
- [ ] Budget creation - the user can create a new budget for a given period along with its expenses and categories
- [ ] Group creation - the user can create a new group which will include a budget and other users
- [ ] Join a group - the user can join an already existing group
- [ ] Report generation - the user can generate a PDF report for a given budget


All of the functions and their connections are explained in detail in the PDF documentation which consists of UML diagrams.


## System implementation

The system is designed using UML diagrams and implemented with the following technologies.

Back end: The back end is created using Express library (Node.js) and MySQL as a database. 

Front end: The front end is created using React.js and Axios to create an API for the back end.


## System visuals

Here are some images of the deployed system.


<img src="https://i.imgur.com/9X0aUJb.png" align="center" height="350" width="650"/>

<img src="https://i.imgur.com/eEdEWtS.png" align="center" height="350" width="650"/>

<img src="https://i.imgur.com/JNKgCMx.png" align="center" height="350" width="650"/>

<img src="https://i.imgur.com/QyPBWvk.png" align="center" height="350" width="650"/>
