const { createTransport } = require('nodemailer');
const db                  = require("./db/dbConn");

const transporter = createTransport({
    host: "smtp-relay.sendinblue.com",
    port: 587,
    auth: {
        user: process.env.EM_USER,
        pass: process.env.EM_PASS,
    },
});


async function send(recipient) {
    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    const code = getRandomInt(100000, 999999);

    const mailOptions = {
        from: process.env.EM_USER,
        to: recipient,
        subject: "Budget tracker - verification",
        html: `<h3>Your verification code is:</h3> <p>${code}</p> <p>Don't share this code with anyone.</p><p>Student budget tracker team, 2023</p>`
    };

    try {
        const info = await transporter.sendMail(mailOptions);
        console.log("Email sent:", info.response);
        await db.saveCode(recipient, code);
    } catch (error) {
        console.log(error);
    }
}

function sendPass(recipient,password) {

    const mailOptions = {
        from: process.env.EM_USER,
        to: recipient,
        subject: "Budget tracker - password reset",
        html: `<h3>Your temporary password is:</h3> <p>${password}</p> 
        <h3>IMPORTANT: THIS IS A TEMPORARY PASSWORD MAKE SURE TO CHANGE THE PASSWORD ONCE YOU LOG IN USING THIS CODE AND A NEW PASSWORD </h3>
        <p>Don't share this password with anyone.</p><p>Student budget tracker team, 2023</p>`
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if(error) {
            console.log(error);
        } else {
            console.log("Email sent: "+ info.response);
        }
    });
}


module.exports = { 
    send,
    sendPass
};
