const express   = require("express");
require('dotenv').config();
const cors      = require("cors")
const app       = express();
const db        = require("./db/dbConn");
const login     = require("./routes/login");
const verify    = require("./routes/verify");
const reset     = require("./routes/reset");
const signup    = require("./routes/signup");
const getbudget = require("./routes/getbudget");
const path      = require("path");
const port = 8092;

app.use(express.urlencoded({extended : true}));
app.use(cors({
  methods:["GET", "POST"],
}))
app.use("/login", login);
app.use("/verify", verify);
app.use("/reset", reset);
app.use("/signup", signup);
app.use("/getbudget", getbudget);
app.use(express.static(path.join(__dirname, "build")))

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "build", "index.html")) 
})


app.listen(port, () => {
    console.log("Server is running.");
    
})