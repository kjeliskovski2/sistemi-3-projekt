const express     = require("express");
const bodyParser  = require('body-parser');
const getbudget   = express.Router();
const db          = require("../db/dbConn");

getbudget.use(bodyParser.json());

getbudget.get("/", (req, res) => {
    res.send("Login");
})

getbudget.post("/", async (req, res, next) => {
    try{
     const id = req.body.id;
     if (id){
         const queryResult  = await db.GetBudget(id)
         const queryResult2 = await db.GetVerify(id)         
         res.send({queryResult, queryResult2});
     } 
     else {
         res.sendStatus(204)
         console.log("Error sending budget!")
     }
     res.end();
    }catch(err){
     console.log(err)
     res.sendStatus(500)
     next()
    }
});

getbudget.post("/log", async (req, res, next) => {
    try{
     const u_id = req.body.u_id;
     const b_id = req.body.b_id;

     if (u_id && b_id){
        const queryResult = await db.GetBudgetLog(b_id);
        const makeLog  = await db.CreateLog(u_id, queryResult[0])
         
         res.send({queryResult});
     } 
     else {
         res.sendStatus(204)
         console.log("Error sending budget!")
     }
     res.end();
    }catch(err){
     console.log(err)
     res.sendStatus(500)
     next()
    }
});


getbudget.post("/create", async(req, res, next) => {
    try{
        const id      = req.body.id;
        const name    = req.body.name;
        const n_cat   = req.body.n_cat;
        const expires = req.body.expires;
        const amount  = req.body.amount;

        if (name && n_cat && expires && amount && id){
            const queryResult=await db.CreateBudget(name, n_cat, expires, amount, id)
        } 
        else {
            res.sendStatus(204)
            console.log("Error creating budget!")
        }
        res.end();
       }catch(err){
        console.log(err)
        res.sendStatus(500)
        next()
       }
});


module.exports = getbudget