const express    = require("express");
const bodyParser = require('body-parser');
const login      = express.Router();
const db         = require("../db/dbConn");

login.use(bodyParser.json());

login.get("/", (req, res) => {
    res.send("Login");
})



login.post("/", async (req, res, next) => {
    try{
     const email = req.body.email;
     const password = req.body.password;
     if (email && password){
         const queryResult=await db.AuthUser(email)        
         if(queryResult.length>0){
             if(password===queryResult[0].password){
                 res.send({logged:true, user:queryResult[0]})
             } else{
                 res.sendStatus(204)
                  console.log("Incorrect credentials")
             }
         } else{
             res.sendStatus(204)
             console.log("User not registered");   
         }
     } 
     else {
         res.sendStatus(204)
         console.log("Please enter email and password!")
     }
     res.end();
    }catch(err){
     console.log(err)
     res.sendStatus(500)
     next()
    }
  });

module.exports = login