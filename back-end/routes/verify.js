const express = require('express');
const bodyParser = require('body-parser');
const emailSender = require("../emailSender.js");
const verify = express.Router();
const db     = require("../db/dbConn");

verify.use(bodyParser.json());

verify.get("/", (req, res) => {
    res.send("Cav");
});

verify.post("/", async (req, res) => {
    var email = req.body.email;
    if (email) {
        console.log("Email sent.");
        emailSender.send(email);
    } else {
        console.log("Email not entered!");
    }
    res.end();
});

verify.post("/code", async (req, res) => {
    var code  = req.body.code;
    var email = req.body.email;

    if(email && code) {
        const queryResult=await db.AuthUser(email)        
        if(queryResult.length>0){
            if(code===queryResult[0].code){
               db.verify(email);
            } else{
                res.sendStatus(204)
                 console.log("Incorrect code!")
            }
        } else{
            res.sendStatus(204)
            console.log("User not registered!");   
        }
    } else {
        console.log("Problem with data!");
    }
    res.end();
});


module.exports = verify;