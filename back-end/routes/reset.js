const express = require('express');
const bodyParser = require('body-parser');
const emailSender = require("../emailSender.js");
const reset = express.Router();
const db = require("../db/dbConn");

reset.use(bodyParser.json());

reset.get("/", (req, res) => {
    res.send("Cav");
});

reset.post("/", async (req, res) => {
    var email = req.body.email;
    if (email) {

        function getRandomInt(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    
        const code = getRandomInt(100000, 999999);

        try {
            let queryResult = await db.saveTempPass(email, code);
            console.log("Temporary password sent.");
            emailSender.sendPass(email, code);
        } catch (err) {
            console.log(err);
            res.sendStatus(500);
        }
    } else {
        console.log("Email not entered!");
    }
    res.end();
});

reset.post("/change", async (req, res) => {
    var id          = req.body.id;
    var oldPassword = req.body.oldPassword;
    var newPassword = req.body.newPassword;
    if(id && oldPassword && newPassword) {
        try {
            let queryResult = await db.getUser(id);
            if(oldPassword === queryResult[0].password) {
                await db.changePw(id, newPassword);
                return res.sendStatus(200)
            } else {
                return res.sendStatus(204);
            }
        } catch(err) {
            return res.sendStatus(500);
        }
    } else {
        return res.sendStatus(204)
    }
})
module.exports = reset;
