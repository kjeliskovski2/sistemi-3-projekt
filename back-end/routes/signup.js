const express = require("express");
const bodyParser = require('body-parser');
const signup    = express.Router();
const db      = require("../db/dbConn");

signup.use(bodyParser.json());

signup.get("/", (req, res) => {
    res.send("Sign up");
})



signup.post("/", async (req, res, next) => {
    try{
     const name = req.body.name;
     const surname = req.body.surname;
     const email = req.body.email;
     const password = req.body.password;
     if (name && surname && email && password){
        await db.AddUser(name, surname, email, password);     
        console.log("Successfully registered!");
     } else {
        res.sendStatus(204);
        console.log("Data missing");
     }
     res.end();
    }catch(err){
     console.log(err)
     res.sendStatus(500)
     next()
    }
  });

module.exports = signup;