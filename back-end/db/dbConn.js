const { rejects } = require('assert');
const express = require('express');
const mysql   = require('mysql2');


const  conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS, 
    database: 'SISIII2024_89211067',
  })


 conn.connect((err) => {
      if(err){
          console.log("ERROR: " + err.message);
          return;    
      }
      console.log('Connection established');
    })
  
let dataPool = {};

dataPool.AuthUser=(email)=> {
      return new Promise ((resolve, reject)=>{
        conn.query('SELECT * FROM User WHERE email = ?', email, (err,res, fields)=>{
          if(err){return reject(err)}
          return resolve(res)
        })
      })  
      
    }

dataPool.getPass=(email)=> {
      return new Promise ((resolve, reject)=>{
        conn.query('SELECT * FROM User WHERE email = ?', email, (err,res, fields)=>{
          if(err){return reject(err)}
          return resolve(res)
        })
      })  
      
    }

dataPool.saveTempPass=(email, code)=> {
      return new Promise ((resolve, reject)=>{
        conn.query('Update User SET password = ? WHERE email = ?', [code, email], (err,res, fields)=>{
          if(err){return reject(err)}
          return resolve(res)
        })
      })  
      
}

dataPool.saveCode = (email, code) => {
      return new Promise((resolve, reject) => {
        conn.query('UPDATE User SET code = ? WHERE email = ?',[code, email],(err, res) => {
          if (err) {
            return reject(err);
          }
            console.log("Saved into DB: "+code);
            return resolve(res);
        }
        );
      });
    };

dataPool.verify = (email) => {
      return new Promise((resolve, reject) => {
        conn.query('UPDATE User SET verified = 1 WHERE email = ?',email,(err, res) => {
          if (err) {
            return reject(err);
          }
            console.log("User verified!");
            return resolve(res);
        }
        );
      });
};

dataPool.AddUser=(name,surname,email,password)=>{
      return new Promise ((resolve, reject)=>{
        conn.query(`INSERT INTO User (name,surname,email,password) VALUES (?,?,?,?)`, [name, surname, email, password], (err,res)=>{
          if(err){return reject(err)}
          return resolve(res)
        })
      })
};

dataPool.GetBudget=(id) => {
  return new Promise((resolve, reject) => {
    conn.query('SELECT * FROM Budget WHERE u_id = ?',id,(err, res) => {
      if (err) {
        return reject(err);
      }
        return resolve(res);
    }
    );
  });
};

dataPool.GetVerify=(id) => {
  return new Promise((resolve, reject) => {
    conn.query('SELECT verified FROM User WHERE id = ?',id,(err, res) => {
      if (err) {
        return reject(err);
      }
        return resolve(res);
    }
    );
  });
}

dataPool.CreateBudget=(name, n_cat, expires, amount, id) => {
  return new Promise((resolve, reject) => {
    conn.query('INSERT INTO Budget (name,n_cat,expires,amount,u_id) VALUES (?,?,?,?,?)', [name, n_cat, expires, amount, id], (err,res) => {
      if(err) {return reject(err)}
      return resolve(res)
    })
  })
};

dataPool.getUser=(id) => {
  return new Promise((resolve, reject) => {
    conn.query('SELECT * FROM User WHERE id = ?', id, (err, res) => {
      if(err) {
        return reject(err);
      }
        return resolve(res);
        console.log("user selected")
    })
  })
};

dataPool.changePw=(id, newPassword) => {
  return new Promise((resolve, reject) => {
    conn.query('UPDATE User SET password = ? WHERE id = ?', [newPassword, id], (err, res) => {
      if(err) {
        return reject(err)
      }
        return resolve(res)
        console.log("password changed")
    })
  })
};

dataPool.GetBudgetLog=(id) => {
  return new Promise((resolve, reject) => {
    conn.query('SELECT * FROM Budget WHERE id = ?', id, (err, res) => {
      if(err) {
        return reject(err)
      }
        return resolve(res)

    })
  })
}

dataPool.CreateLog=(u_id, queryResult) => {
  const b_id = queryResult.id
  const startingA = queryResult.amount

  return new Promise((resolve, reject) => {
    conn.query('INSERT INTO Log (startingA, totalA, u_id, b_id) VALUES (?,?,?,?)', [startingA, 500, u_id, b_id], (err, res) => {
      if(err) {
        return reject(err)
      }
        return resolve(res)

    })
  })
}
module.exports = dataPool;